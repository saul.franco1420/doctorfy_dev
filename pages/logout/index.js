import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Logout } from "@/redux/actions/login";
import { useRouter } from "next/router";

/**
 * Index Component
 *
 * This component is responsible for logging out the user and redirecting to the login page.
 * It dispatches the Logout action upon mounting and then navigates to the login route.
 */
const Index = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  // Effect hook to handle logout and redirection
  useEffect(() => {
    // Dispatch the Logout action to initiate the logout process
    dispatch(Logout());

    // Redirect to the login page after logout
    router.push("/login");
  }, [dispatch, router]);

  return <div>Cerrando sesión...</div>;
};


export default Index;
