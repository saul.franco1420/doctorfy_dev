import React from "react";
import Sidebar from "@/components/Sidebar";
import PrivateRoute from "@/components/PrivateRoute";

const Index = () => {
  return (
    <>
      <PrivateRoute>
        <Sidebar />
        <main className="lg:pl-72">
          <div className="xl:pl-96">
            <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-5">
              <p>Dashboard</p>
            </div>
          </div>
        </main>
      </PrivateRoute>
    </>
  );
};

export default Index;
