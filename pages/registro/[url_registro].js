import React, { useState } from "react";
import Head from "next/head";
import { Header } from "@/components/Header";
import { useRouter } from "next/router";
import styles from "@/styles/style_pages/registro.module.css";
import Button from "@/components/Buttons";
import TermsConditions from "./Sections/termsCondition";
import SessionText from "./Sections/sesionText";
import ViewSection from "./Sections/viewSection";
import FormSection from "./Sections/formSection";
import CardRegistroDoctor from "@/components/Cards/Sections/CardRegistroDoctor";
import Success from "@/components/Section/Success";

export const getStaticPaths = async () => {
  // Fetch data from the API to retrieve the list of paciente' registration URLs
  const res = await fetch(`${process.env.API}/registroPaciente`);

  // Parse the JSON response from the API
  const response = await res.json();

  // Create an array of 'paths' for dynamic routing based on the fetched data
  const paths = response?.map((registro) => ({
    params: { url_registro: registro.url_registro },
  }));

  // Specify 'fallback: false' to indicate that unmatched paths should result in a 404
  return { paths, fallback: false };
};

export const getStaticProps = async (context) => {
  // Extract the 'url_registro' parameter from the context
  const url_registro = context.params.url_registro;

  // Fetch data from the API based on the 'url_registro' parameter
  const res = await fetch(`${process.env.API}/registroPaciente?url_registro=${url_registro}`);

  // Parse the JSON response from the API
  let dataRegistro = await res.json();

  // Extract the first object from the array of data
  dataRegistro = dataRegistro[0];

  // Return the extracted data as props to be used in the component
  return { props: { dataRegistro } };
};


const RegistrationForm = ({ dataRegistro }) => {
  const router = useRouter();

  const {
    registro_url_after,
    inputs_registro,
    label_registro,
    sublabel_registro,
    styled_form,
    style_input,
    name_button,
    text_view,
    terms_conditions,
    init_sesion,
    icon_view,
    name_registro_website,
    view_doctor,
    checkbox_registro,
    buttons_registro,
  } = dataRegistro;

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleClick = () => {
    router.push(registro_url_after);
  };

  return (
    <>
      <Head>
        <title>Paciente</title>
        <meta
          name="description"
          content="En doctorfy el registrarte es totalmente gratis"
        />
        <link rel="icon" href="/images/logo_blue.svg" />
      </Head>

      <section className={styles[styled_form]}>
          {(() => {
            switch (name_registro_website) {
              case "inicio":
              case "informacion":
                return (
                  <div className={styles.login}>
                    <div className={styles.form_login}>
                      <FormSection
                        label_registro={label_registro}
                        sublabel_registro={sublabel_registro}
                        inputs_registro={inputs_registro}
                        formData={formData}
                        handleChange={handleChange}
                        viewDoctor={view_doctor}
                        checkboxRegistro={checkbox_registro}
                        buttonsRegistro={buttons_registro}
                        styleInput={style_input}

                      />
                      <TermsConditions termsConditions={terms_conditions} />

                      <div
                        className={styles.section_button_form}
                        style={{ marginTop: view_doctor && "5vh" }}
                      >
                        <Button
                          text={name_button}
                          onClick={handleClick}
                          typeCss="button_register"
                        />
                        <SessionText sessionText={init_sesion} />
                      </div>
                    </div>
                    <ViewSection text_view={text_view} icon_view={icon_view} />
                  </div>
                );
              case "confirmacion":
                return (
                  <div className={styles.login}>
                    <div className={styles.form_login}>
                      <FormSection
                        label_registro={label_registro}
                        sublabel_registro={sublabel_registro}
                        inputs_registro={inputs_registro}
                        formData={formData}
                        handleChange={handleChange}
                        viewDoctor={view_doctor}
                        checkboxRegistro={checkbox_registro}
                        buttonsRegistro={buttons_registro}
                        styleInput={style_input}
                      />
                      <CardRegistroDoctor nameUrl={name_registro_website} />
                      <div
                        className={styles.section_button_form}
                        style={{ marginTop: view_doctor && "5vh" }}
                      >
                        <Button
                          text={name_button}
                          onClick={handleClick}
                          typeCss="button_register"
                        />
                      </div>
                      <SessionText sessionText={init_sesion} />
                    </div>
                    <ViewSection text_view={text_view} icon_view={icon_view} />
                  </div>
                );
              case "reservacion":
                return (
                  <div className={styles.login}>
                    <Header typeHeader={"dashboard"} />;
                    <div className={styles.form_login}>
                      <FormSection
                        label_registro={label_registro}
                        sublabel_registro={sublabel_registro}
                        inputs_registro={inputs_registro}
                        formData={formData}
                        handleChange={handleChange}
                        viewDoctor={view_doctor}
                        checkboxRegistro={checkbox_registro}
                        buttonsRegistro={buttons_registro}
                        styleInput={style_input}
                      />
                      <div
                        className={styles.section_button_form}
                        style={{ marginTop: view_doctor && "5vh" }}
                      >
                        <Button
                          text={name_button}
                          onClick={handleClick}
                          typeCss="button_register"
                        />
                        <SessionText sessionText={init_sesion} />
                      </div>
                    </div>
                    <ViewSection text_view={text_view} icon_view={icon_view} />
                  </div>
                );

              case "agendar":
                return (
                  <div className={styles.login}>
                    <div className={styles.form_login}>
                      <FormSection
                        label_registro={label_registro}
                        sublabel_registro={sublabel_registro}
                        inputs_registro={inputs_registro}
                        formData={formData}
                        handleChange={handleChange}
                        viewDoctor={view_doctor}
                        checkboxRegistro={checkbox_registro}
                        buttonsRegistro={buttons_registro}
                        styleInput={style_input}
                      />
                      <CardRegistroDoctor nameUrl={name_registro_website} />

                      <div
                        className={styles.section_button_form}
                        style={{ marginTop: view_doctor && "5vh" }}
                      >
                        <Button
                          text={name_button}
                          onClick={handleClick}
                          typeCss="button_register"
                        />
                      </div>
                      <SessionText sessionText={init_sesion} />
                    </div>
                    <ViewSection text_view={text_view} icon_view={icon_view} />
                  </div>
                );
              case "agenda_exitosa":
                return <Success />;
              default:
                return (
                  <>
                    {" "}
                   
                  </>
                );
            }
          })()}
      </section>
    </>
  );
};

export default RegistrationForm;
