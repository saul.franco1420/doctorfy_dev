import styles from "@/styles/style_pages/registro.module.css";

const TermsConditions = ({ termsConditions }) => {
  return (
    <>
      {" "}
      {termsConditions && (
        <p className={styles.text_register}>
          Acepto los{" "}
          <strong className={styles.strong_register}>
            Terminos y Condiciones
          </strong>{" "}
          asi como también las{" "}
          <strong className={styles.strong_register}>
            Politicas de Privacidad
          </strong>
        </p>
      )}
    </>
  );
};

export default  TermsConditions;
