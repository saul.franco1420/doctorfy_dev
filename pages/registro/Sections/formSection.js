import React, { useState } from "react";
import styles from "@/styles/style_pages/registro.module.css";
import { InputText } from "@/components/Inputs/InputRegister";
import { Checkbox } from "@/components/Inputs/Checkbox";
import { ButtonForm } from "@/components/Inputs/ButtonForm";
import { PrincipalDoctor } from "@/components/Cards/Sections/PrincipalDoctor";

const FormSection = ({
  label_registro,
  sublabel_registro,
  inputs_registro,
  formData,
  handleChange,
  viewDoctor,
  checkboxRegistro,
  buttonsRegistro,
  styleInput,
}) => {
  const inputsCheckbox = checkboxRegistro?.inputs_registro || [];
  const inputsButton = buttonsRegistro?.inputs_registro || [];

  const [selectedOption, setSelectedOption] = useState(null);
  const [selectButton, setSelectedButton] = useState(null);

  const handleCheckboxChange = (option) => {
    setSelectedOption(option);
  };

  const handleOptionButton = (optionValue) => {
    setSelectedButton(optionValue);
  };


  return (
    <>
      <div className={styles.section_text_form}>
        <h1 className={styles.title_register}>{label_registro}</h1>
        <p className={styles.subtitle_register}>{sublabel_registro} </p>
      </div>
      <div className={styles.form_section}>
    
        {/*CARD*/}
        {viewDoctor && <PrincipalDoctor />}
        {/*CARD*/}

        {/*BUTTON*/}
        {inputsButton.length > 1 && (
          <div className={styles.section_button}>
            {inputsButton?.map((field) => (
              <ButtonForm
                key={field.name}
                name={field.name}
                label={field.placeholder}
                selectButton={selectButton}
                handleSelect={handleOptionButton}
              />
            ))}
          </div>
        )}
        {/*BUTTON*/}

        {/*INPUT TEXT*/}
        {!viewDoctor &&
          inputs_registro?.map((field) => (
            <InputText
              key={field.name}
              field={field}
              formData={formData}
              handleChange={handleChange}
              label={field.label}
              styleInput={field.style_input}
            />
          ))}
        {/*INPUT TEXT*/}

        {/*CHECKBOX*/}
        {inputsCheckbox.length > 1 && (
          <p className={styles.label_checkbox}> {checkboxRegistro?.label} </p>
        )}

        {inputsCheckbox.length > 1 && (
          <div className={styles.section_checkbox}>
            {inputsCheckbox?.map((field) => (
              <Checkbox
                key={field.name}
                name={field.name}
                label={field.label}
                checked={selectedOption === field.name}
                handleChange={() => handleCheckboxChange(field.name)}
                labelPosition={"start"}
                styleInput={field.style_input}
              />
            ))}
          </div>
        )}
        {/*CHECKBOX*/}
      </div>
    </>
  );
};

export  default FormSection;
