import styles from "@/styles/style_pages/registro.module.css";

const SessionText = ({ sessionText }) => {
  return (
    <>
      {" "}
      {sessionText && (
        <p className={styles.text_account_register}>
          Ya tienes cuenta?{" "}
          <strong className={styles.strong_account_register}>
            Iniciar Sesión
          </strong>
        </p>
      )}
    </>
  );
};

export default SessionText;
