import { Header } from "@/components/Header";
import { Search } from "@/components/LandingPage/Search";
import { ImageSection } from "@/components/LandingPage/ImageSection";
import { CardSection } from "@/components/LandingPage/CardSection";
import { ImageSecondSection } from "@/components/LandingPage/ImageSecondSection";
import { Testimonials } from "@/components/LandingPage/Testimonials";
import { SectionNumber } from "@/components/LandingPage/SectionNumber";
import { Registration } from "@/components/LandingPage/Registration";
import { Footer } from "@/components/Footer";

const index = () => {
  return (
    <>
      <Header typeHeader={'public'} />
      <ImageSection />
      <Search/>
      <CardSection/>
      <ImageSecondSection/>
      <Testimonials/>
      <SectionNumber/>
      <Registration/>
      <Footer/>
    </>
  );
};

export default index;
