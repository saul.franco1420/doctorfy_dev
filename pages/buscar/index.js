import { Doctor } from '@/components/Cards/Doctor';
import { Filter } from '@/components/Filters';
import { Header } from '@/components/Header';
import styles from '@/styles/style_pages/buscar.module.css'

const Index = () => {
    return ( 
        <>
            <section className={styles.buscar}>
                <Header />
                <Filter />

                <div className={styles.data_doctor}>
                    <Doctor />
                </div>
            </section>
        
        </>
     );
}
 
export default Index;