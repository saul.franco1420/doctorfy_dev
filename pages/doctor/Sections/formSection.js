import React, { useState } from "react";
import Image from "next/image";
import styles from "@/styles/style_pages/registro.module.css";
import { InputText } from "@/components/Inputs/InputRegister";
import { Checkbox } from "@/components/Inputs/Checkbox";
import { ButtonForm } from "@/components/Inputs/ButtonForm";
import { PrincipalDoctor } from "@/components/Cards/Sections/PrincipalDoctor";

const FormSection = ({
  label_registro,
  sublabel_registro,
  inputs_registro,
  formData,
  handleChange,
  viewDoctor,
  checkboxRegistro,
  buttonsRegistro,
  iconStep,
  iconStepName,
  orientation,
}) => {
  const inputsCheckbox = checkboxRegistro?.inputs_registro || [];
  const inputsButton = buttonsRegistro?.inputs_registro || [];

  const [selectedOption, setSelectedOption] = useState(null);
  const [selectButton, setSelectedButton] = useState(null);

  const handleCheckboxChange = (option) => {
    setSelectedOption(option);
  };

  const handleOptionButton = (optionValue) => {
    setSelectedButton(optionValue);
  };

  return (
    <div className={styles.section_text_form_doctor}>
      {/*STEP*/}
      {iconStep && orientation === "horizontal" && (
        <div className={ iconStepName === 'congratulations.svg' ?  styles.step_image_doctor_exitoso :  styles.step_image_doctor}>
          <Image
            src={`/images/${iconStepName}`}
            alt="doctorfy"
            layout="responsive"
            width={42}
            height={42}
            priority
          />
        </div>
      )}
      {/*STEP*/}

      <div className={styles.section_text_doctor}>
        <h1 className={styles.title_register_doctor}>{label_registro}</h1>
        <p className={styles.subtitle_register_doctor}>{sublabel_registro} </p>
      </div>
      <div
        className={
          checkboxRegistro?.textAlign === "start"
            ? styles.form_section_doctor
            : styles.section_form_input
        }
        
      >
        {/*CARD*/}
        {viewDoctor && <PrincipalDoctor />}
        {/*CARD*/}

        {/*STEP*/}
        {iconStep && orientation === "vertical" && (
          <div className={styles.section_step}>
            <Image
              src={`/images/${iconStepName}`}
              alt="doctorfy"
              layout="responsive"
              width={242}
              height={350}
              priority
            />
          </div>
        )}
        {/*STEP*/}

        {/*BUTTON*/}
        {inputsButton.length > 1 && (
          <div className={styles.section_button_doctor}>
            {inputsButton?.map((field) => (
              <ButtonForm
                key={field.name}
                name={field.name}
                label={field.placeholder}
                selectButton={selectButton}
                handleSelect={handleOptionButton}
              />
            ))}
          </div>
        )}
        {/*BUTTON*/}

        {/*CHECKBOX*/}
        {inputsCheckbox.length > 1 && (
          <p className={styles.label_checkbox}> {checkboxRegistro?.label} </p>
        )}

        {inputsCheckbox.length > 1 && (
          <div className={styles.section_checkbox_doctor}>
            {inputsCheckbox?.map((field) => (
              <Checkbox
                key={field.id}
                name={field.name}
                label={field.label}
                checked={selectedOption === field.name}
                handleChange={() => handleCheckboxChange(field.name)}
                labelPosition={"end"}
                styleInput={field.style_input}
              />
            ))}
          </div>
        )}
        {/*CHECKBOX*/}

        {/*INPUT TEXT*/}
        {!viewDoctor &&
          inputs_registro?.map((field) => (
            <InputText
              key={field.name}
              field={field}
              formData={formData}
              handleChange={handleChange}
              label={field.label}
              styleInput={field.style_input}
            />
          ))}
        {/*INPUT TEXT*/}
      </div>
    </div>
  );
};

export default FormSection;
