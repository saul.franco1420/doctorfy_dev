import React from 'react';
import Image from 'next/image';
import styles from "@/styles/style_pages/registro.module.css";

const ViewSection = ({ text_view, icon_view }) => {
  return (
    <div className={styles.view_login_doctor}>
      {text_view && <p className={styles.text_view_login_doctor}>{text_view}</p>}
      {icon_view && (
        <div className={styles.image_view_register_doctor}>
          <Image
            src={`/images/${icon_view}`}
            alt="doctorfy"
            layout="responsive"
            width={200}
            height={200}
            priority
          />
        </div>
      )}
      <div
        className={
          icon_view
            ? styles.image_register_without_image_doctor
            : styles.image_register_with_image
        }
      >
        <Image
          src="/images/logo.svg"
          alt="Logo of doctorfy"
          width={200}
          height={100}
          priority
        />
      </div>
    </div>
  );
};

export default ViewSection;
