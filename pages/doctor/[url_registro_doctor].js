import React, { useState } from "react";
import Image from "next/image";
import Head from "next/head";
import { Header } from "@/components/Header";
import { useRouter } from "next/router";
import styles from "@/styles/style_pages/registro.module.css";
import Button from "@/components/Buttons";
import TermsConditions  from "./Sections/termsCondition";
import SessionText  from "./Sections/sesionText";
import ViewSection  from "./Sections/viewSection";
import FormSection  from "./Sections/formSection";
import CardRegistroDoctor  from "@/components/Cards/Sections/CardRegistroDoctor";

export const getStaticPaths = async () => {
  // Fetch data from the API to retrieve the list of doctors' registration URLs
  const res = await fetch(`${process.env.API}/registroDoctor`);

  // Parse the JSON response from the API
  const response = await res.json();

  // Create an array of 'paths' for dynamic routing based on the fetched data
  const paths = response?.map((registro) => ({
    params: { url_registro_doctor: registro.url_registro_doctor },
  }));

  // Specify 'fallback: false' to indicate that unmatched paths should result in a 404
  return { paths, fallback: false };
};

export const getStaticProps = async (context) => {
  // Extract the 'url_registro_doctor' parameter from the context
  const url_registro_doctor = context.params.url_registro_doctor;

  // Fetch data from the API based on the 'url_registro_doctor' parameter
  const res = await fetch(`${process.env.API}/registroDoctor?url_registro_doctor=${url_registro_doctor}`);

  // Parse the JSON response from the API
  let dataRegistroDoctor = await res.json();

  // Extract the first object from the array of data
  dataRegistroDoctor = dataRegistroDoctor[0];

  // Return the extracted data as props to be used in the component
  return { props: { dataRegistroDoctor } };
};



const Index = ({ dataRegistroDoctor }) => {
  const router = useRouter();

  const {
    registro_url_after,
    inputs_registro,
    label_registro,
    sublabel_registro,
    styled_form,
    style_input,
    name_button,
    text_view,
    terms_conditions,
    init_sesion,
    icon_view,
    name_registro_website,
    view_doctor,
    checkbox_registro,
    buttons_registro,
    icon_step,
    icon_step_name,
    orientation,
  } = dataRegistroDoctor;


  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleClick = () => {
    router.push(registro_url_after);
  };

  return (
    <>
      <Head>
        <title>Doctor</title>
        <meta
          name="description"
          content="En doctorfy el registrarte es totalmente gratis"
        />
        <link rel="icon" href="/images/logo_blue.svg" />
      </Head>

      {name_registro_website === "reservacion" && (
        <Header typeHeader={"dashboard"} />
      )}

      <section className={styles[styled_form]}>
        <div className={styles.login}>
          <div
            className={styles.form_login_doctor}
            style={{
              justifyContent:
                name_registro_website === "perfil" ? "start" : "center",
            }}
          >
            <FormSection
              label_registro={label_registro}
              sublabel_registro={sublabel_registro}
              inputs_registro={inputs_registro}
              formData={formData}
              handleChange={handleChange}
              viewDoctor={view_doctor}
              checkboxRegistro={checkbox_registro}
              buttonsRegistro={buttons_registro}
              iconStep={icon_step}
              iconStepName={icon_step_name}
              orientation={orientation}
              styleInput={style_input}
            />

            {name_registro_website === "imagen_perfil" && (
              <div className={styles.upload_perfil}>
                <Image
                  src={`/images/upload_document.svg`}
                  alt="doctorfy"
                  layout="responsive"
                  width={40}
                  height={40}
                  priority
                />
              </div>
            )}

            {name_registro_website === "sobre_ti" && (
              <div className={styles.section_sobre_ti}>
                <textarea className={styles.sobre_ti_text}>
                  ¿Cuantos años tienes de experiencia?
                   ¿Cual es tu filosofia
                  hacia el cuidado al paciente? ¿Cuales son tus logros?
                </textarea>
              </div>
            )}

            <TermsConditions termsConditions={terms_conditions} />
            {name_registro_website === "confirmacion" && (
              <CardRegistroDoctor nameUrl={name_registro_website} />
            )}
            {name_registro_website === "agendar" && (
              <CardRegistroDoctor nameUrl={name_registro_website} />
            )}

            <div className={styles.section_button_doctor} style={{ marginTop: view_doctor && "5vh" }}>
              <Button
                text={name_button}
                onClick={handleClick}
                typeCss="button_register"
              />
            </div>

            <SessionText sessionText={init_sesion} />
          </div>

          <ViewSection text_view={text_view} icon_view={icon_view} />
        </div>
      </section>
    </>
  );
};

export default Index;
