import React from "react";
import Sidebar from "@/components/Sidebar";
import PrivateRoute from "@/components/PrivateRoute";
import ContactCards from "@/components/Tailwind/ContactCards";

const MedicosGuardados = () => {
  return (
    <PrivateRoute>
      <Sidebar />
      <main className="lg:pl-72">
        <div className="xl:pl-96">
          <div className="px-4 py-4 sm:px-6 lg:px-8 lg:py-5">
            <strong className="mb-8">Médicos Guardados</strong>
            <ContactCards />
          </div>
        </div>
      </main>
    </PrivateRoute>
  );
};

export default MedicosGuardados;
