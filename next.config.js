/** @type {import('next').NextConfig} */

module.exports = {
  images: {
    domains: ["images.unsplash.com"],
  },
  env: {
    API_URL: process.env.API_URL,
    API_KEY: process.env.API_KEY,
    API_FLY: process.env.API_FLY,
  },
};
