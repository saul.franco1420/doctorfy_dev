export const cardData = [
    {
      number: "1",
      title: "Médicos Evaluados",
      description:
        "Los médicos que conforman la red Doctorfy son cuidadosamente aprobados tras cumplir nuestros estándares y evaluaciones.",
    },
    {
      number: "2",
      title: "Comodidad Sin Costos",
      description:
        "Nuestro servicio garantiza comodidad, facilidad y seguridad al reservar tu cita médica, completamente gratis.",
    },
    {
      number: "3",
      title: "Todas Las Especialidades",
      description:
        "Ya sea que estés buscando a un especialista en un área determinada o simplemente un médico de familia de confianza.",
    },
];

export const testimonialData = [
  {
    imageSrc: "/images/avatar_11.svg",
    primaryText: "El doctor Hector y su clínica me regresaron la vida de un síndrome de intestino irritable. Totalmente recomendable",
    secondaryText: "Dr. Hector Avila (Gastroenterólogo)",
    tertiaryText: "Veronica Vera Castillo",
  },
  {
    imageSrc: "/images/avatar_22.svg",
    primaryText: "La mejor doctora y excelente atención!!! Siempre me dio seguimiento y me explicó muy bien para los cuidados posteriores",
    secondaryText: "Dra. Sofia Vergara",
    tertiaryText: "José Bernardo Flores",
  },
  {
    imageSrc: "/images/avatar_33.svg",
    primaryText: "Excelente atención y profesionalismo, muy acertado.",
    secondaryText: "Dr. Ismael Mendoza",
    tertiaryText: "Angel Khalil Cruz",
  },
];

export const numberInfo = [
  { number: 4000, title: "Especialistas" },
  { number: 2000, title: "Reservas Al Mes" },
  { number: 40, title: "Especialidades" },
  { number: 50000, title: "Visitas en Doctorfy.com" },
  { number: 24, title: "Atención Al Cliente" },
];