import {
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
} from "@heroicons/react/24/outline";

export const navigation = (currentNavItem) => {
  return [
    {
      name: "Inicio",
      href: "/dashboard",
      icon: HomeIcon,
      current: currentNavItem === "Inicio",
    },
    {
      name: "Medicos Guardados",
      href: "/medicos",
      icon: UsersIcon,
      current: currentNavItem === "Medicos Guardados",
    },
    {
      name: "Mis Citas",
      href: "/citas",
      icon: FolderIcon,
      current: currentNavItem === "Mis Citas",
    }
  ];
};

export const teams = [
  {
    id: 1,
    name: "Agregar Consultorio",
    href: "/consultorio",
    initial: "+",
    current: false,
  },
];

export const userNavigation = [
  { name: "Mi Perfil", href: "/perfil" },
  { name: "Cerrar Sesión", href: "/logout" },
];
