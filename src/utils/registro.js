import styles from "@/styles/style_pages/registro.module.css";

export const inputRegister = [
  {
    name: "name",
    placeholder: "Nombre Completo",
    className: styles.input_form,
    tailwind:
      "block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6",
  },
  {
    name: "email",
    placeholder: "Correo Electrónico",
    className: styles.input_form,
    tailwind:
      "block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6",
  },
  {
    name: "password",
    placeholder: "Contraseña",
    className: styles.input_form,
    tailwind:
      "block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6",
  },
];

export const doctorInfoData = [
  {
    iconSrc: "/images/icon_calendar.svg",
    text: "22 Junio 2023",
  },
  {
    iconSrc: "/images/icon_time.svg",
    text: "14:30 (Horario Ciudad de México)",
  },
  {
    iconSrc: "/images/icon_map.svg",
    text: "Av. del Ejido 3, Xocotlán, Texcoco. Estado de México 56236",
  },
];


export const doctorInfoDataAgendar = [
  {
    icon: "/images/icon_indication.svg",
    text: "Indicaciones para llegar: Nos encontramos en el piso 8. Edificio de urgencias."
  },
  {
    icon: "/images/icon_money.svg",
    text: "Transferencia, efectivo y pago con tarjeta."
  },
  {
    icon: "/images/icon_calendar_register.svg",
    text: "22 Junio 2023"
  },
  {
    icon: "/images/icon_time_register.svg",
    text: "10:00 A.M (Horario Ciudad de México)"
  }
];
