import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "@/styles/styles_components/header.module.css";
import Button from "@/components/Buttons";
import { Select } from "@/components/Inputs/Select";
import { speciality, state } from "@/utils/components/inputs/inputsRegister";

const Header = ({ typeHeader }) => {

  const router      = useRouter();
  const handleClick = (url) => router.push(url);

  return (
    <section
      className={
        typeHeader === "public"
          ? styles.section_header
          : styles.section_header_dashboard
      }
    >
      <section className={styles.select_header}>
        {typeHeader === "public" ? (
          <>
            <div className={styles.image_header}>
              <Image
                src={
                  typeHeader === "public"
                    ? "/images/logo.svg"
                    : "/images/logo_blue.svg"
                }
                alt="Logo of doctorfy"
                width={200}
                height={200}
                priority
              />
            </div>
            <div className={styles.section_select}>
              <div className={styles.select_div}>
                <p className={styles.text_header}>Soy Doctor</p>
                <Image
                  src="/images/arrow_down.svg"
                  alt="arrow down"
                  width={22}
                  height={22}
                  priority
                  style={{
                    marginLeft: 5,
                  }}
                />
              </div>
              <div className={styles.select_div}>
                <p className={styles.text_header}>
                  <Link href="/registro/inicio">Registrarme</Link>
                </p>
              </div>
            </div>
            <div className={styles.divider} />
          </>
        ) : (
          <div className={styles.input_search}>
            <Select
              options={speciality}
              placeHolder="Selecciona una opción"
              name="speciality"
              label=""
              defaultValue="option1"
            />
            <Select
              options={state}
              placeHolder="Selecciona una opción"
              name="speciality"
              label=""
              defaultValue="option1"
            />
            <Button
              text=""
              onClick={()=>handleClick('/search')}
              typeCss={"button_register"}
              icon={"search.svg"}
              buttonWidth={"40px"}
              buttonHeight={"37px"}
            />
          </div>
        )}
        <div className={styles.button_init}>
          <Button
            text="Iniciar sesión"
            onClick={()=>handleClick('/login')}
            typeCss={
              typeHeader === "public" ? "button_default" : "button_register"
            }
            buttonWidth={"180px"}
          />
        </div>
      </section>
    </section>
  );
};

export { Header };
