import Button from "@/components/Buttons";
import styles from "@/styles/styles_components/landing_doctor.module.css";
import { useRouter } from "next/router";


const Search = () => {

  const router = useRouter();

  const handleClick = (url) => router.push(url);

  return (
    <>
      <section className={styles.section_search_principality}>
        <div className={styles.search_section}>
          <div className={styles.select_section}>
            <select
              id="location"
              name="location"
              className={`mt-2 block w-full rounded-md items-center border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6 ${styles.select_search}`}
              defaultValue="Especialidad / Enfermedad / Nombre Del Doctor"
            >
              <option>Especialidad / Enfermedad / Nombre Del Doctor</option>
              <option>Canada</option>
              <option>Mexico</option>
            </select>
          </div>
          <div className={styles.select_section_state}>
            <select
              id="location"
              name="location"
              className={`mt-2 block w-full rounded-md items-center border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6 ${styles.select_search}`}
              defaultValue="Ciudad de México"
            >
              <option>Ciudad de México</option>
              <option>Canada</option>
              <option>Mexico</option>
            </select>
          </div>
          <div className={styles.button_section}>
            <Button
              text="Buscar"
              onClick={() => handleClick('/buscar')}
              typeCss={"button_search"}
              icon={"search.svg"}
            />
          </div>
        </div>
      </section>
    </>
  );
};

export  { Search };
