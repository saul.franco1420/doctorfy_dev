import React, { useState } from "react";
import { numberInfo } from "@/utils/landingPage";
import { useInView } from "react-intersection-observer";
import CountUp from "react-countup";
import styles from "@/styles/styles_components/landing_doctor.module.css";


const SectionNumber = () => {

  const [animationTriggered, setAnimationTriggered] = useState(false);
  const { ref, inView } = useInView({
    triggerOnce: true,
    threshold: 0.5, 
  });

  if (inView && !animationTriggered) {
    setAnimationTriggered(true);
  }

  return (
    <section className={styles.section_number_data}>
      <div ref={ref} className={styles.section_number}>
        {numberInfo?.map((info, index) => (
          <div className={styles.information_number} key={index}>
            <strong className={styles.text_number}>
              <CountUp
                start={animationTriggered ? 0 : null}
                end={info.number}
                duration={2}
                separator=","
              />
            </strong>
            <p className={styles.text_title_number}>{info.title}</p>
          </div>
        ))}
      </div>
    </section>
  );
};

export { SectionNumber };
