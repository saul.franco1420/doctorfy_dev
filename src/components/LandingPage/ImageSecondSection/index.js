import Image from "next/image";
import styles from "@/styles/styles_components/landing_doctor.module.css";

const ImageSecondSection = () => {
  return (
    <section className={styles.section_second}>
      <div className={styles.image_secondary}>
        <Image
          src="/images/portada_3.svg"
          alt="doctorfy"
          width={500}
          height={300}
          style={{
            width:'100%'
          }}
        />
      </div>
      <div className={styles.image_third_complement}>
        <Image
          src="/images/section_third_complement.svg"
          alt="image of doctors"
          priority
          quality={100}
          layout="responsive"
          width={500}
          height={300}
        />
      </div>
    </section>
  );
};

export { ImageSecondSection };
