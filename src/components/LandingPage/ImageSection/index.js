import Image from "next/image";
import styles from "@/styles/styles_components/landing_doctor.module.css";

const ImageSection = () => {
  return (
    <>
      <section className={styles.section_first}>
        <div className={styles.image_container}>
          <Image
            src="/images/portada.svg"
            alt="doctorfy"
            width={500}
            height={300}
            style={{
              width:'100%'
            }}
          />
          <div className={styles.section_text}>
            <h1 className={styles.text_h1}>
              Encuentra a Tu Médico De Confianza
            </h1>
          </div>
        </div>
        <div className={styles.image_container_mobile}>
          <Image
            src="/images/doctor_mobile_2.svg"
            alt="image of doctors"
            width={500}
            height={500}
            style={{
              width: '100%'
            }}
          />
        </div>
      </section>
    </>
  );
};

export { ImageSection };
