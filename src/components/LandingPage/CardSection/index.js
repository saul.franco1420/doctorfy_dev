import React from "react";
import Image from "next/image";
import styles from "@/styles/styles_components/landing_doctor.module.css";
import { cardData } from '@/utils/landingPage'


const CardSection = () => {
  return (
    <section className={styles.section_card}>
      <p className={styles.title_card}>
        Médicos de <strong className={styles.title_blue_card}>primer nivel</strong> a tu disposición
      </p>
      <p className={styles.subtitle_card}>
        En Doctorfy, priorizamos la calidad y pasión por el cuidado al paciente - Reserva sin ningún costo.
      </p>

      <div className={styles.information_card}>
        {cardData?.map((card, index) => (
          <div className={styles.card} key={index}>
            <div className={styles.rounded_number}>{card.number}</div>
            <p className={styles.text_card}>{card.title}</p>
            <p className={styles.container_card}>{card.description}</p>
            <a className={styles.link_reserve}>
              Reservar Cita{" "}
              <Image src="/images/left.svg" alt="left icon" width={20} height={20} />
            </a>
          </div>
        ))}
      </div>


      <div className={styles.image_section}>
        <Image src="/images/image_section.svg" alt="left icon" width={293} height={29} />
      </div>

    </section>
  );
};

export { CardSection };
