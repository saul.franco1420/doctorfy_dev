import Image from "next/image";
import styles from "@/styles/styles_components/landing_doctor.module.css";

const starArray = Array(5).fill(null);

const Testimonial = ({
  imageSrc,
  primaryText,
  secondaryText,
  tertiaryText,
}) => (
  <div className={styles.testimonial_card}>
    <div className={styles.image_testimonial}>
      <Image
        src={imageSrc}
        alt="avatar"
        priority
        quality={100}
        width={100}
        height={100}
      />
    </div>
    <div className={styles.star_testimonial}>
      {starArray?.map((_, index) => (
        <div key={index}>
          <Image
            src="/images/star.svg"
            alt="star"
            priority
            quality={100}
            width={17}
            height={17}
          />
        </div>
      ))}
    </div>
    <div className={styles.text_testimonial}>
      <p className={styles.text_primary_testimonial}>{primaryText}</p>
      <p className={styles.text_secondary_testimonial}>{secondaryText}</p>
      <p className={styles.text_third_testimonial}>{tertiaryText}</p>
    </div>
  </div>
);

export { Testimonial };
