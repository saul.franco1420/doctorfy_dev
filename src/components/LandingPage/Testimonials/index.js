import styles from '@/styles/styles_components/landing_doctor.module.css';
import { testimonialData } from "@/utils/landingPage";
import { Testimonial } from '@/components/LandingPage/Testimonials/Testimonial'


const Testimonials = () => (
    <section className={styles.section_card_three}>
      <p className={styles.title_card}>
        Opiniones mas recientes de <strong className={styles.title_blue_card}>pacientes</strong>
      </p>
      <p className={styles.subtitle_card}>Miles de pacientes, enfrentando diversas situaciones de salud, recurren a Doctorfy</p>
      <div className={styles.testimonial_section}>
        {testimonialData?.map((testimonial, index) => (
          <Testimonial key={index} {...testimonial} />
        ))}
      </div>
    </section>
);

export { Testimonials };
