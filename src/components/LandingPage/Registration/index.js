import styles from "@/styles/styles_components/landing_doctor.module.css";
import Button from "@/components/Buttons";

const Registration = () => {
  const handleClick = () => {
    console.log("buscando...");
  };
  return (
    <section className={styles.section_registration}>
      <div className={styles.card_registration}>
        <div className={styles.button_registration}>
          <Button
            text="¿Eres Especialista?"
            onClick={handleClick}
            typeCss={"button_especialista"}
          />
        </div>
        <p className={styles.text_registration}>
          ¡Unete a la comunidad{" "}
          <strong className={styles.text_strong_registration}> Doctorfy!</strong>
        </p>
        <p className={styles.text_secondary_registration}>Solicita tu evaluación completamente gratis</p>
      </div>
    </section>
  );
};

export { Registration };
