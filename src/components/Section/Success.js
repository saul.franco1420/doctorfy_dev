import styles from "@/styles/styles_components/section.module.css";
import Button from "@/components/Buttons";
import Image from "next/image";

const Success = () => {
  const handleClick = () => {
    console.log("Send...");
  };

  return (
    <section className={styles.section_success}>
      <div className={styles.success}>
        <div className={styles.first_success}>
          <Image
            src="/images/check.svg"
            alt="Check"
            width={164}
            height={81}
            className={styles.image_success}
          />
        </div>
        <div className={styles.second_success}>
          <p className={styles.title_success}>
            ¡Tu reservación está confirmada!
          </p>
          <p className={styles.subtitle_success}>
            Te enviaremos un correo de confirmación y una notificación horas
            antes de tu cita.
          </p>
          <Button
            text={"Ver mis citas"}
            onClick={handleClick}
            typeCss="button_register"
          />
        </div>
      </div>
    </section>
  );
};

export default Success;
