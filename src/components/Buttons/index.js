import React from "react";
import PropTypes from "prop-types";
import styles from "@/styles/styles_components/button.module.css";
import Image from "next/image";
import ReactLoading from 'react-loading';

const Button = ({
  text,
  onClick,
  type,
  typeCss,
  icon,
  iconEnd,
  buttonWidth,
  buttonHeight,
  loading
}) => {
  const buttonStyle = {
    width: buttonWidth,
    height: buttonHeight
  };

  return (
    <button
      className={styles[typeCss]}
      onClick={onClick}
      type={type}
      style={buttonStyle}
      disabled={loading}
    >
      {loading ? (
        // Use the Spin component when loading is true
        <ReactLoading type={'spin'} color={"#FFF"} height={20} width={20} />
        ) : (
        <>
          {icon && (
            <span>
              <Image src={`/images/${icon}`} alt="Icon" width={20} height={20} />
            </span>
          )}
          {text}

          {iconEnd && (
            <span>
              <Image src={`/images/${iconEnd}`} alt="Icon" width={15} height={5} />
            </span>
          )}
        </>
      )}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(["button", "submit", "reset"]),
  typeCss: PropTypes.string.isRequired,
  icon: PropTypes.string,
  loading: PropTypes.bool
};

Button.defaultProps = {
  type: "button",
  loading: false
};

export default Button;
