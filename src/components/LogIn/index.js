import styles                       from '@/styles/login.module.css'

const Index = () => {
  return (
    <>
      <p className={styles.title_section}>
        Tu bienestar, la clave de la felicidad.
      </p>
    </>
  );
};

export default Index;
