import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { verifyLogin } from '@/redux/actions/login';
import { useDispatch } from "react-redux";
import { StatusLogin } from "@/redux/selector/login";



const PrivateRoute = ({ children }) => {

  const dispatch = useDispatch();
  const router = useRouter();
  const { login } = StatusLogin();

  useEffect(() => {
    const token = login || sessionStorage.getItem('login');
    const currentPath = router.pathname;

    if (currentPath !== '/login') {
        // Verify the login status first
        dispatch(verifyLogin(token));
    }

    // Once the login status is verified, decide whether to redirect
    if (!token) router.push('/login');
    
  }, [login]);


  return <>{children}</>;
};

export default PrivateRoute;
