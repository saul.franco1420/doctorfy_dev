import React from 'react';
import Image from 'next/image';

const StarRating = ({ count, icon }) => {
  const starArray = Array.from({ length: count }, (_, index) => index);

  return (
    <>
      {starArray?.map((starIndex) => (
        <Image
          key={starIndex}
          src={`/images/${icon}`}
          alt="star"
          width={20}
          height={20}
          priority
        />
      ))}
    </>
  );
};

export  { StarRating };
