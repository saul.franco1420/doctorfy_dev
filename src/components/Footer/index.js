import React from "react";
import Image from "next/image";
import styles from "@/styles/styles_components/footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.section_footer}>
        <div className={styles.image_footer}>
          <Image
            src="/images/logo_footer.svg"
            alt="Logo of doctorfy"
            width={200}
            height={100}
            priority
          />
        </div>

        <div className={styles.section_tab_footer}>
          <p className={styles.tab_active_footer}>Inicio</p>
          <p className={styles.tab_footer}>Terminos y Condiciones</p>
          <p className={styles.tab_footer}>P de P</p>
          <p className={styles.tab_footer}>Contacto</p>
        </div>

        <div className={styles.image_flag_footer}>
          <Image
            src="/images/spain_flag.svg"
            alt="spanish "
            width={43}
            height={43}
            priority
          />
          <div className={styles.select_languaje_footer}>
            <p className={styles.text_languaje_footer}>Español</p>
            <Image
              src="/images/vector_down.svg"
              alt="spanish "
              width={11}
              height={5}
              priority
            />
          </div>
        </div>
      </div>
      <div className={styles.copyright_footer}>
        <Image
          src="/images/copyright.svg"
          alt="spanish "
          width={18}
          height={18}
          priority
          style={{
            marginRight: 5,
          }}
        />
        <p className={styles.text_copyright}>
          2023 copyrights by{" "}
          <strong className={styles.text_strong_copyright}>
            Doctorfy S.A. V.C.{" "}
          </strong>
          All Rights Reserved.
        </p>
      </div>
      <div className={styles.text_copyright_mobile}>
        <Image
          src="/images/copyright.svg"
          alt="spanish "
          width={18}
          height={18}
          priority
          style={{
            marginRight: 5,
          }}
        />
        <p>2023 Doctorfy S.A. De S.V.</p>
        <div className={styles.tab_foter_mobile}>
          <p>
            Términos{" "}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="2"
              height="2"
              viewBox="0 0 2 2"
              fill="none"
              style={{
                marginLeft:'8px'
              }}
            >
              <circle cx="1" cy="1" r="1" fill="#666666" />
            </svg>
          </p>
          <p>
            Privacidad
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="2"
              height="2"
              viewBox="0 0 2 2"
              fill="none"
              style={{
                marginLeft:'8px'
              }}
            >
              <circle cx="1" cy="1" r="1" fill="#666666" />
            </svg>
          </p>
          <p>Contacto</p>
        </div>
      </div>
    </footer>
  );
};

export { Footer };
