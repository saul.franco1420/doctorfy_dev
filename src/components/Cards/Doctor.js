import styles from "@/styles/styles_components/cards.module.css"
import { PrincipalDoctor } from "./Sections/PrincipalDoctor"
import { Calendary } from '@/components/Calendary'


const Doctor = () => {
  return (
    <div className={styles.doctor}>
      <PrincipalDoctor positionCard={'absolute'} leftPosition={'20vw'} />
      <Calendary />
    </div>
  );
};

export { Doctor };
