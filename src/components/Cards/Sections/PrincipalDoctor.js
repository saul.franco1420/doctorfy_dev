import styles from "@/styles/styles_components/cards.module.css";
import Image from "next/image";
import { StarRating } from "@/components/Assets/Icons/Star";

const PrincipalDoctor = ({ positionCard, leftPosition }) => {
  const positionStyle = {
    position: positionCard,
    left: leftPosition,
  };
  return (
    <>
      <section className={styles.section_doctor} style={positionStyle}>
        <div className={styles.first_section_doctor}>
          <div className={styles.image_doctor}>
            <Image
              src={`/images/image_doctor.svg`}
              alt="doctor"
              width={154}
              height={154}
            />
          </div>
          <div className={styles.text_section_doctor}>
            <div className={styles.text_icon_doctor}>
              <p className={styles.text_name_doctor}>
                Dr. Jose Sebastian Flores Vera{" "}
                <Image
                  src={`/images/icon_checked.svg`}
                  alt="doctor"
                  width={15}
                  height={15}
                  style={{
                    position: "relative",
                    top: "-20px",
                    left: "50px",
                  }}
                />
              </p>
              <Image
                src={`/images/icon_marker.svg`}
                alt="doctor"
                width={21}
                height={21}
                style={{
                  marginLeft: "30px",
                  marginTop: "-30px",
                }}
              />
            </div>
            <p className={styles.text_speciality}>
              Dermatóloga <strong>Ver más</strong>
            </p>
            <div className={styles.star_comentary}>
              <StarRating count={5} icon={"star_blue.svg"} />
              <p className={styles.text_star_comentary}>80 comentarios</p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export { PrincipalDoctor };
