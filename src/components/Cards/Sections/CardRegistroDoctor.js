import styles from "@/styles/styles_components/cards.module.css";
import Image from "next/image";
import { DoctorInfo } from "./Doctor/DoctorInfo";
import { doctorInfoData, doctorInfoDataAgendar } from "@/utils/registro";
import DoctorInfoAgendar from "./Doctor/DoctorInfoAgendar";

const CardRegistroDoctor = ({ nameUrl }) => {
  return (
    <>
      <section className={styles.section_doctor_registro}>
        <div className={styles.first_section_doctor}>
          <div className={styles.image_doctor}>
            <Image
              src={`/images/image_doctor.svg`}
              alt="doctor"
              width={154}
              height={154}
            />
          </div>
          <div className={styles.text_section_doctor}>
            <div className={styles.text_icon_doctor}>
              <p className={styles.text_name_doctor}>
                Dr. Jose Sebastian Flores Vera{" "}
              </p>
            </div>
            <p className={styles.text_speciality}>Dermatóloga</p>
          </div>
        </div>

        <div className={styles.second_section_registro_doctor}>
          {nameUrl === "confirmacion" &&
            doctorInfoData?.map((info, index) => (
              <DoctorInfo key={index} iconSrc={info.iconSrc} text={info.text} />
            ))}
          {nameUrl === "agendar" &&
            doctorInfoDataAgendar?.map((item, index) => (
              <DoctorInfoAgendar
                key={index}
                iconSrc={item.icon}
                text={item.text}
              />
            ))}
        </div>
      </section>
    </>
  );
};

export default CardRegistroDoctor ;
