import React from 'react';
import styles from '@/styles/styles_components/cards.module.css';
import Image from 'next/image';

const DoctorInfoAgendar = ({ iconSrc, text }) => (
  <div className={styles.section_text_agendar}>
    <Image src={iconSrc} alt="doctor" width={25} height={25} />
    <p className={styles.text_card_registro}>{text}</p>
  </div>
);

export default DoctorInfoAgendar;
