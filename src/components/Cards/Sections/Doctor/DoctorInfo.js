import React from "react";
import styles from "@/styles/styles_components/cards.module.css";
import Image from "next/image";

const DoctorInfo = ({ iconSrc, text }) => (
  <div className={styles.section_text_doctor}>
    <Image src={iconSrc} alt="doctor" width={15} height={15} />
    <p className={styles.text_card_registro}>{text}</p>
  </div>
);

export { DoctorInfo };
