import styles from '@/styles/styles_components/filter.module.css'
import Image from "next/image";
import Button from "@/components/Buttons";


const Filter = () => {

    const handleClick = () => {
        console.log("Iniciar Sesión...");
    };


    return ( 
        <div className={styles.filter}>
            <section className={styles.section_filter}>
                <div className={styles.section_text_button_filter}>
                    <div className={styles.text_icon_filter}>
                        <Image src={`/images/icon_map_filter.svg`} alt="Icon Map" width={23} height={23} />
                        <p className={styles.text_filter}>
                            Dermatólogos en Ciudad de México
                        </p>
                    </div>
                    <div className={styles.buttons_filter}>
                        <Button
                            text="Seguros"
                            onClick={handleClick}
                            typeCss={'button_white_whit_border'}
                            icon={'icon_seguros.svg'}
                            iconEnd={'arrow_down_gray.svg'}
                            buttonWidth={"160px"}
                            buttonHeight={"42px"}
                        />
                        <Button
                            text="Más Filtros"
                            onClick={handleClick}
                            typeCss={'button_white_whit_border'}
                            icon={'icon_more_filter.svg'}
                            iconEnd={'arrow_down_gray.svg'}
                            buttonWidth={"160px"}
                            buttonHeight={"42px"}
                        />

                    </div>

                </div>
            </section>
        </div>
     );
}
 
export  { Filter };