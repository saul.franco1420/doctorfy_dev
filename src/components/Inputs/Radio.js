import React from 'react';
import styles from '@/styles/styles_components/inputs.module.css';

const Radio = ({ name, value, label, checked, handleChange }) => {
  return (
    <div className={styles.radioContainer}>
      <input
        type="radio"
        name={name}
        id={`${name}-${value}`}
        value={value}
        checked={checked}
        className={styles.radio}
        onChange={handleChange}
      />
      {label && (
        <label className={styles.radioLabel} htmlFor={`${name}-${value}`}>
          {label}
        </label>
      )}
    </div>
  );
};

export { Radio };
