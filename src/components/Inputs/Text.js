import styles from "@/styles/styles_components/inputs.module.css";

const Text = ({ field, placeholder, name ,formData, handleChange, label, styleInput, type }) => (
  <div className={styles.section_input}>
    {label && (
      <label className={styles.label} htmlFor={label}>
        {label}
      </label>
    )}

    <input
      type={type}
      name={name}
      id={name}
      className={`${field.tailwind}  ${styles[styleInput]}`}
      placeholder={placeholder}
      value={formData[name]}
      onChange={handleChange}
      autoComplete="new-password"
    />
  </div>
);

export default Text ;
