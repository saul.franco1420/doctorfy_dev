import React from "react";
import styles from "@/styles/styles_components/inputs.module.css";

const Checkbox = ({ name, label, checked, handleChange, labelPosition, styleInput }) => {
  return (
    <div className={styles.section_checkbox}>
      {label && labelPosition === "start" && (
        <label className={styles.checkbox_label} htmlFor={name}>
          {label}
        </label>
      )}
      <input
        type="checkbox"
        name={name}
        id={name}
        checked={checked}
        className={`${styles.checkbox}  ${styles[styleInput]}`}
        onChange={handleChange}
      />
      {label && labelPosition === "end" && (
        <label className={styles.checkbox_label} htmlFor={name}>
          {label}
        </label>
      )}
    </div>
  );
};

export { Checkbox };
