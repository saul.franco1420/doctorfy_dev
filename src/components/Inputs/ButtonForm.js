import React from "react";
import styles from "@/styles/styles_components/inputs.module.css";

const ButtonForm = ({ name, label, selectButton, handleSelect }) => {
  const handleClick = () => {
    // Llama a la función handleSelect solo si el botón no está ya seleccionado
    if (selectButton !== name) {
      handleSelect(name);
    }
  };
  return (
    <button
      className={`${styles.button_form} ${
        selectButton === name ? styles.button_form_select : ""
      }`}
      onClick={handleClick}
    >
      {label}
    </button>
  );
};

export { ButtonForm };
