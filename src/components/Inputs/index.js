import InputText from '@/components/Inputs/InputRegister'

const Index = ({ typeInput }) => {
  return ( 
    <>
      { typeInput === 'text' && <InputText/>}
    </>
   );
}
 
export default Index;