import styles from "@/styles/styles_components/inputs.module.css";

const InputText = ({ field, formData, handleChange, label, styleInput }) => (
  <div className={styles.section_input}>
    {label && (
      <label className={styles.label} htmlFor={label}>
        {label}
      </label>
    )}

    <input
      type={field.name === "password" ? "password" : "text"}
      name={field.name}
      id={field.name}
      className={`${field.tailwind}  ${styles[styleInput]}`}
      placeholder={field.placeholder}
      value={formData[field.name]}
      onChange={handleChange}
      autoComplete="new-password"
    />
  </div>
);

export { InputText };
