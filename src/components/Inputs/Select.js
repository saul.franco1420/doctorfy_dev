import React from "react";
import styles from "@/styles/styles_components/inputs.module.css";

const Select = ({ options, placeHolder, name, label }) => {
  return (
    <>
      {label && (
        <label className={styles.label} htmlFor={name}>
          {label}
        </label>
      )}
      <select
        name={name}
        id={name}
        className={styles.select}
      >
        <option value="" disabled>
          {placeHolder}
        </option>
        {options?.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </>
  );
};

export { Select };
