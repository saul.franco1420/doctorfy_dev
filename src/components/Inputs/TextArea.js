// TextArea.js

import React from 'react';
import styles from '@/styles/styles_components/inputs.module.css';

const TextArea = ({ name, label, placeholder, value, handleChange }) => {
  return (
    <>
      {label && (
        <label className={styles.label} htmlFor={name}>
          {label}
        </label>
      )}
      <textarea
        name={name}
        id={name}
        className={styles.textarea}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
      />
    </>
  );
};

export { TextArea };
