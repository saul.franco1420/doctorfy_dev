import {
    PACIENTE_CREATE_ERROR,
    PACIENTE_CREATE_LOADING,
    PACIENTE_CREATE_SUCCESS
  } from '../type/paciente'

// Actions to get all Paciente
export const initCreatePaciente = () => ({
    type: PACIENTE_CREATE_LOADING,
  });
  
  export const successCreatePaciente = (data) => ({
    type: PACIENTE_CREATE_SUCCESS,
    payload: data,
  });
  
  export const errorCreatePaciente = (error) => ({
    type: PACIENTE_CREATE_ERROR,
    payload: error,
  });
  
  // Asynchronous action to create Paciente
  export function createPaciente(data) {
    return async (dispatch) => {
      dispatch(initCreatePaciente());
      try {
        // Make POST request to the API with the provided data as the request body
        const response = await clientAxios.post(`/paciente`, data);
  
        // Dispatch success action with the retrieved data
        dispatch(successCreatePaciente(response.data));
        
      } catch (error) {
        // Dispatch error action and display an alert
        console.error("Error: ", error);
        dispatch(errorCreatePaciente(error));
      }
    };
  }