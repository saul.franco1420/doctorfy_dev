
import {
    PACIENTE_CREATE_ERROR,
    PACIENTE_CREATE_LOADING,
    PACIENTE_CREATE_SUCCESS
  } from '../type/paciente'
  
  const initialState = {
    paciente: [],
    loading: false,
    error: false,
  };
  
  const pacienteReducer = (state = initialState, action) => {
    switch (action.type) {
  
      case PACIENTE_CREATE_LOADING:
        return {
          ...state,
         loading: true,
         error: false
        };
  
      case PACIENTE_CREATE_ERROR:
        return{
          ...state,
          loading: false,
          error: action.payload
        }
  
      case PACIENTE_CREATE_SUCCESS:
        return{
          ...state,
          loading: false,
          error: false,
          paciente: action.payload
        }
        
      default:
        return state;
    }
  };
  
  export default pacienteReducer;