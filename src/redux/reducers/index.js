import pacienteReducer from './paciente';
import loginReducer from './login';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
    paciente: pacienteReducer,
    login: loginReducer
});

export default rootReducer;