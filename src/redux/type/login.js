// LOGIN ACCESS
export const LOGIN_ACCESS_LOADING               = 'LOGIN_ACCESS_LOADING'
export const LOGIN_ACCESS_SUCCESS               = 'LOGIN_ACCESS_SUCCESS'
export const LOGIN_ACCESS_ERROR                 = 'LOGIN_ACCESS_ERROR'

//LOGOUT 
export const LOGOUT_SUCCESS                     = 'LOGOUT_SUCCESS'

//VERIFI LOGIN
export const VERIFY_LOGIN_LOADING               = 'VERIFY_LOGIN_LOADING'
export const VERIFY_LOGIN_SUCCESS               = 'VERIFY_LOGIN_SUCCESS'
export const VERIFY_LOGIN_ERROR                 = 'VERIFY_LOGIN_ERROR' 






